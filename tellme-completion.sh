#/usr/bin/env bash


_tellme_completions()
{
  local opts
  opts="device-tree gstreamer jetson ssh update v4l2 i2c"

  op_device_tree="from-system from-binaries"

  op_gstreamer="config pipelines"
  op_gstreamer_config="plugin-path"
  op_gstreamer_pipelines="min-nvarguscamerasrc min-v4l2 min-usb"

  op_jetson="init make copy flash view-release"
  op_jetson_make="menu kernel dtbs modules modules-install"
  op_jetson_copy="var kernel dtbs modules"
  op_jetson_flash="all kernel dtbs"

  op_ssh="generate-keys"

  op_i2c="detect read8 write8 read16 write16"

  op_v4l2="formats fps compliance debug-enable debug-cat current-config"

  case $COMP_CWORD in
    1)
      COMPREPLY=( $(compgen -W "${opts}" -- "${COMP_WORDS[COMP_CWORD]}") )
      ;;
    2)
      if [ ${COMP_WORDS[$COMP_CWORD-1]} == 'device-tree' ]; then
        COMPREPLY=( $(compgen -W "${op_device_tree}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-1]} == 'gstreamer' ]; then
        COMPREPLY=( $(compgen -W "${op_gstreamer}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-1]} == 'jetson' ]; then
        COMPREPLY=( $(compgen -W "${op_jetson}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-1]} == 'ssh' ]; then
        COMPREPLY=( $(compgen -W "${op_ssh}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-1]} == 'v4l2' ]; then
        COMPREPLY=( $(compgen -W "${op_v4l2}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-1]} == 'i2c' ]; then
        COMPREPLY=( $(compgen -W "${op_i2c}" -- "${COMP_WORDS[COMP_CWORD]}") )
      fi
      ;;
    3)
      if [ ${COMP_WORDS[$COMP_CWORD-2]} == 'gstreamer' ] && [ ${COMP_WORDS[$COMP_CWORD-1]} == 'config' ]; then
        COMPREPLY=( $(compgen -W "${op_gstreamer_config}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-2]} == 'gstreamer' ] && [ ${COMP_WORDS[$COMP_CWORD-1]} == 'pipelines' ]; then
        COMPREPLY=( $(compgen -W "${op_gstreamer_pipelines}" -- "${COMP_WORDS[COMP_CWORD]}") )
      fi

      if [ ${COMP_WORDS[$COMP_CWORD-2]} == 'jetson' ] && [ ${COMP_WORDS[$COMP_CWORD-1]} == 'make' ]; then
        COMPREPLY=( $(compgen -W "${op_jetson_make}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-2]} == 'jetson' ] && [ ${COMP_WORDS[$COMP_CWORD-1]} == 'copy' ]; then
        COMPREPLY=( $(compgen -W "${op_jetson_copy}" -- "${COMP_WORDS[COMP_CWORD]}") )
      elif [ ${COMP_WORDS[$COMP_CWORD-2]} == 'jetson' ] && [ ${COMP_WORDS[$COMP_CWORD-1]} == 'flash' ]; then
        COMPREPLY=( $(compgen -W "${op_jetson_flash}" -- "${COMP_WORDS[COMP_CWORD]}") )
      fi
      ;;
  esac
  return 0
}

complete -F _tellme_completions tellme