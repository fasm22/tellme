#/usr/bin/env bash

echo "Downloading needed files..."
wget -q https://gitlab.com/fasm22/tellme/raw/master/tellme
wget -q https://gitlab.com/fasm22/tellme/raw/master/tellme-completion.sh

echo "Installing tellme..."
chmod +x tellme 
sudo cp tellme /usr/bin
echo "Configuring auto-complete..."
sudo cp tellme-completion.sh /etc/bash_completion.d/tellme
. /etc/bash_completion

echo "Deleting tmp files..."
rm tellme
rm tellme-completion.sh
rm -f rrtellme
rm -f install.sh